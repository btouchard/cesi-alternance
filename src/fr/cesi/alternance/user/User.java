package fr.cesi.alternance.user;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

import fr.cesi.alternance.helpers.Entity;
//import fr.cesi.alternance.Promo;

public class User extends Entity implements Parcelable {

	private String name;
	private String mail;
	private String token;
	private String role;
	private String phone;
	private String pwd;

	public User() {
		name = "Julien Dos Santos";
		mail = "julien.dossantos@viacesi.fr";
		phone = "0626355295";
	}
	
	public User(Parcel in){
		id = in.readLong();
		name = in.readString();
		mail = in.readString();
		token = in.readString();
		role = in.readString();
		phone = in.readString();
		pwd = in.readString();
	}

	@Override
	public String getApiPath() {
		String api_path = "user/" + this.id;
		return api_path;
	}

	@Override
	public User fromJSON(JSONObject json) {
		try {
			id = json.getLong("id");
			name = json.getString("name");
			mail = json.getString("email");
			phone = json.getString("phone");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return this;
	}

	@Override
	public JSONObject asJSON() {
		return null;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeString(name);
		dest.writeString(mail);
		dest.writeString(token);
		dest.writeString(role);
		dest.writeString(phone);
		dest.writeString(pwd);
	}

	public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
		public User createFromParcel(Parcel in) {
			return new User(in);
		}

		public User[] newArray(int size) {
			return new User[size];
		}
	};

}
