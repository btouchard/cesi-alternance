package fr.cesi.alternance.docs;

import java.util.ArrayList;
import java.util.List;

import fr.cesi.alternance.R;
import fr.cesi.alternance.R.menu;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class DocListActivity extends ListActivity {

	private Boolean selection;
	private DocsAdapter adapter;
	private ArrayList <Doc> docs;
	private boolean remove;


	// ---------------------------------------------------------------------------------------
	// Procedures onCreate 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.doc_list_activity);
		testListDoc();
		getListView().setOnItemLongClickListener(docLongClick);
		getListView().setOnItemClickListener(docClick);
		selection = false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getMenuInflater().inflate(R.menu.menu_doc_list, menu);

		MenuItem item =  menu.findItem(R.id.doc_delete);
		item.setVisible(false);
		return true;
	}

	/*@Override
	public void onCreateContextMenu(ContextMenu menu, View view,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, view, menuInfo);
		menu.add(Menu.NONE, 0, Menu.NONE, R.string.menu_contextuel_delete);
		menu.add(Menu.NONE, 1, Menu.NONE, R.string.menu_contextuel_cancel);
	}*/


	// ---------------------------------------------------------------------------------------
	// Procedures menu

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.doc_delete :
			AlertDialog.Builder builder = new AlertDialog.Builder(this);

			builder.setIcon(android.R.drawable.ic_dialog_alert);
			builder.setTitle(R.string.menu_contextuel_title);
			builder.setNegativeButton(R.string.menu_contextuel_cancel, new DialogInterface.OnClickListener() {	
				@Override
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			builder.setPositiveButton(R.string.menu_contextuel_delete, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					remove = true;
				}
			});
			builder.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
					if (remove) {
						List<Doc> rm = new ArrayList<Doc>();
						for(Doc doc : docs)
							if(doc.isSelected()) rm.add(doc);

						for (Doc doc : rm) 
							docs.remove(doc);
						adapter.notifyDataSetChanged();
						afficheMenuDelete();
						invalidateOptionsMenu();
						deleteDocs();
					}
					remove = false;
				}
			});

			builder.create();
			builder.show();

			return true;
		case R.id.doc_add : 
			return true;

		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem item =  menu.findItem(R.id.doc_delete);
		if(afficheMenuDelete())
			item.setVisible(true);
		else item.setVisible(false);

		return true;
	}

	// -----------------------------------------------------------------------------------------------------
	// Procedure gestion des clics

	private OnItemClickListener docClick = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int position,long id) {
			if(selection){
				if(checkItem(position))
					view.setBackgroundColor(Color.CYAN);
				else view.setBackgroundColor(Color.WHITE);
				invalidateOptionsMenu();
			}
		}
	};

	private OnItemLongClickListener docLongClick = new OnItemLongClickListener() {

		@Override
		public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
			selection = true;

			if(checkItem(position))
				view.setBackgroundColor(Color.CYAN);
			else view.setBackgroundColor(Color.WHITE);

			invalidateOptionsMenu();
			return true;
		}
	};


	// --------------------------------------------------------------------------------------------
	// Fonctions utiles

	private boolean checkItem(int position) {
		Doc doc = ((DocsAdapter) getListAdapter()).getItem(position);
		doc.setSelected(!doc.isSelected());
		docs.get(position).setSelected(doc.isSelected());

		return doc.isSelected();
	}

	private boolean afficheMenuDelete() {
		for(int i = 0; i < docs.size(); i++){
			Doc doc = docs.get(i);
			Log.v("selection", ""+doc.isSelected()+i);
			if(doc.isSelected())
				return true;
		}

		selection = false;
		return false;
	}

	private void traceSel() {
		SparseBooleanArray sel = getListView().getCheckedItemPositions();
		for(int i = 0;i<adapter.getCount();i++){
			Log.v("select" + i, "" +sel.get(i));
		}
	}

	private void deleteDocs() {
		final ProgressDialog progress = ProgressDialog.show(DocListActivity.this, getString(R.string.progress_delete_title), getString(R.string.progress_delete_infos));

		new Thread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					Thread.sleep(3000);
					progress.dismiss();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	// ------------------------------------------------------------------------------------
	// Corps de l'activity

	private void testListDoc() {
		// TODO Auto-generated method stub
		docs = new ArrayList <Doc>();
		for(int i =0;i<10;i++) {
			Doc d = new Doc("name" + i,"desc" + i,"\\path\\" + i);
			docs.add(d);
		}
		adapter = new DocsAdapter(this,R.layout.doc_row,docs);
		setListAdapter(adapter);
	}


	// -------------------------------------------------------------------------------------
	// Class adapter view

	private class DocsAdapter extends ArrayAdapter<Doc>{

		private Context mContext;
		private int mRessource;
		private List<Doc> mItems;

		public DocsAdapter(Context context, int resource,
				List<Doc> objects) {
			super(context, resource, objects);
			// TODO Auto-generated constructor stub
			mContext = context;
			mRessource = resource;
			mItems = objects;
		}
		@Override
		public View getView(int position, View view, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (view == null) view = LayoutInflater.from(mContext).inflate(mRessource, parent, false);

			Doc doc = mItems.get(position);
			TextView tv = (TextView) view.findViewById(R.id.titre);
			tv.setText(doc.getName());
			tv = (TextView) view.findViewById(R.id.desc);
			tv.setText(doc.getDescription());

			if(doc.isSelected())
				view.setBackgroundColor(Color.CYAN);
			else view.setBackgroundColor(Color.WHITE);

			return view;
		}

		@Override
		public Doc getItem(int position) {
			return mItems.get(position);
		}
	}

}
