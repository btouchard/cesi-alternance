package fr.cesi.alternance.docs;

import org.json.JSONObject;

import fr.cesi.alternance.helpers.Entity;

public class Doc extends Entity {
	
	private String name;
	private String description;
	private String path;
	private boolean selected;
	
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}



	
	public Doc (String name, String description, String path){
		this.name = name;
		this.description = description;
		this.path = path;
		this.selected = false;
	}

	@Override
	public String getApiPath() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Doc fromJSON(JSONObject json) {
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	public JSONObject asJSON() {
		// TODO Auto-generated method stub
		return null;
	}

}
