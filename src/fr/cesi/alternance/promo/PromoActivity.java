package fr.cesi.alternance.promo;
import android.os.Bundle;
import fr.cesi.alternance.R;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class PromoActivity extends Activity {
	public static final String TAG = "PromoListActivity";
	private String role;
	private int id_promo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_promo);
		
		id_promo = getIntent().getExtras().getInt("id_promo");		
		
		TextView tv = (TextView) findViewById(R.id.promo_name);
		tv.setText("RIL " + id_promo);
		tv.setText("code promo " + id_promo);
	
		
		listGoTo();
	}

	private void listGoTo() {
		
		final String menu1 = "El�ves";
		final String menu2 = "Intervenants";
		final String menu3 = "Documents";
		final String menu4 = "Supports";
		
		final String[] values = new String[] { menu1, menu2, menu3, menu4};
		
		
		ListView lv = (ListView) findViewById(R.id.goTo);

		lv.setAdapter(new ArrayAdapter<String>(PromoActivity.this,  android.R.layout.simple_list_item_1, values));

		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> promo_toGo, View view, int position,
					long id) {
				
				String menuChoisie = values[position].toString();
				
				try {
					Intent intent = new Intent(PromoActivity.this, TestActivity.class);
					if (menuChoisie.equals(menu1)) 
						intent.setClass(PromoActivity.this, TestActivity.class);
					else if (menuChoisie.equals(menu2))
						intent.setClass(PromoActivity.this, TestActivity.class);
					else if (menuChoisie.equals(menu3))
						intent.setClass(PromoActivity.this, TestActivity.class);
					else if (menuChoisie.equals(menu4))
						intent.setClass(PromoActivity.this, TestActivity.class);
					
					//intent.putExtra("role", role);
					intent.putExtra("id_promo", id_promo);
					startActivity(intent);
					finish();
				} catch (Exception e) {
					Toast.makeText(PromoActivity.this, "Erreur lancement activity " + e , Toast.LENGTH_LONG).show();
				}

				
				
					
			}
		});

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.promo, menu);
		return true;
	}

}
