package fr.cesi.alternance.promo;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.kolapsis.utils.HttpData;
import com.kolapsis.utils.HttpData.HttpDataException;

import fr.cesi.alternance.Constants;
import fr.cesi.alternance.api.Api;
import fr.cesi.alternance.helpers.AccountHelper;
import fr.cesi.alternance.user.User;

public class Promo {
	private int id;
	private int name;
	private int number;
	private String code;
	

	public Promo(int id) {
		this.id = id;
	}
	
	public Promo(int id, int name, int number, String code) {
		this.id = id;
		this.name = name;
		this.number = number;
		this.code = code;
	}
	
	public int getId() {
		return id;
	}
	
	public int getName() {
		return name;
	}
	public void setName(int name) {
		this.name = name;
	}
	
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	public ArrayList<User> getListUser(String role) {
		ArrayList<User> user_list = new ArrayList<User>();
		// Remplacer par list d'�l�ve ou intervenant
		String url = Constants.BASE_API_URL + "/user/listUser";
		try {
			// Prod :
			// String token = AccountHelper.getData(Api.UserColumns.TOKEN);
			// Debug :
			String token = Constants.DEBUG_APP_AUTH_TOKEN;
			
			JSONObject json = new HttpData(url).header(Api.APP_AUTH_TOKEN, token).data("promo", String.valueOf(id)).data("role", role).get().asJSONObject();
			if(json.getBoolean("success")) {
				JSONArray result = json.getJSONArray("result");
				for (int i = 0; i < result.length(); i++) {
					User e = new User().fromJSON(result.getJSONObject(i));
					user_list.add(e);
				}
			}
		} catch (HttpDataException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return user_list;
	}
}
