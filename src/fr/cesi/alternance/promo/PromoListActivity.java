package fr.cesi.alternance.promo;
import java.util.ArrayList;
import java.util.List;

import fr.cesi.alternance.HomeActivity;
import fr.cesi.alternance.R;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class PromoListActivity extends ListActivity{

	private String role;
	private int id_training;
	public static final String TAG = "PromoListActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		//id_training = getIntent().getExtras().getInt("id_training");
		id_training = 12;
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_promo_list);
		getListPromo();
				
	}
	
	private void getListPromo() {
			
		final ArrayList<Promo> listPromo = new ArrayList<Promo>();
		
		for (int i=0; i<10; i++){
			Promo p = new Promo(i);
			p.setName(1+i);
			p.setNumber(2+i);
			p.setCode("CODE" + i );
			Log.v(TAG, "ok" + i);
			listPromo.add(p);
		}
		
		PromoAdapter adapter= new PromoAdapter(this, R.layout.activity_promo_row, listPromo);
		
		setListAdapter(adapter);
	
		
		ListView lv = getListView();
		
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> list, View view, int position,
					long id) {
				
				Intent intent = new Intent(PromoListActivity.this, PromoActivity.class);
				intent.putExtra("id_promo", listPromo.get(position).getId());
				startActivity(intent);
				finish();
			}
		});
	}


	private class PromoAdapter extends ArrayAdapter<Promo> {

		private Context mContext;
		private int mResource;
		private List<Promo> mItems;
		
		public PromoAdapter(Context context, int resource,
				List<Promo> objects) {
			super(context, resource, objects);	
			
			mContext = context;
			mResource = resource;
			mItems = objects;
		}
		
		@Override
		public View getView(int position, View view, ViewGroup parent) {
			if (view == null) 
				view = LayoutInflater.from(mContext).inflate(mResource, parent, false);
			
			Promo promo = mItems.get(position);
			ImageView iv = (ImageView) view.findViewById(R.id.img);
			TextView tv = (TextView) view.findViewById(R.id.title);
			iv.setImageResource(android.R.drawable.btn_default);
			tv.setText(String.valueOf(promo.getName()));
			tv = (TextView) view.findViewById(R.id.info);
			tv.setText(promo.getCode());
			
			return view;
		}
		
		
		
		
	}
	
}
