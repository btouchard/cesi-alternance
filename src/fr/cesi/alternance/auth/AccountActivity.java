package fr.cesi.alternance.auth;

import com.kolapsis.utils.StringUtils;

import fr.cesi.alternance.Constants;
import fr.cesi.alternance.R;
import fr.cesi.alternance.api.Api;
import fr.cesi.alternance.helpers.AccountHelper;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class AccountActivity extends AccountAuthenticatorActivity {
	
	public static final String TAG = Constants.APP_NAME + ".AccountActivity";

	private Context mContext;
	private AccountManager mAccountManager;
	private String mUsername;
	private String mPassword;
	private Boolean mRequestNewAccount, mConfirmCredentials;
	
	private EditText mEmail, mPwd;
	private Button mSubmit;
	private TextView mError;
	private ProgressBar mProgress;
	private LoginTask mAuthTask;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_auth);
		
		mContext = this;
		mAccountManager = AccountManager.get(mContext);
		
		final Intent intent = getIntent();
		mUsername = intent.getStringExtra(Authenticator.PARAM_USERNAME);
		mRequestNewAccount = mUsername == null;
		mConfirmCredentials = intent.getBooleanExtra(Authenticator.PARAM_CONFIRM_CREDENTIALS, false);
		
		mEmail = (EditText) findViewById(R.id.email);
		mPwd = (EditText) findViewById(R.id.pwd);
		mSubmit = (Button) findViewById(R.id.submit);
		mError = (TextView) findViewById(R.id.error);
		mProgress = (ProgressBar) findViewById(R.id.progress);
		
		mEmail.setText(!StringUtils.isEmpty(mUsername) ? mUsername : Constants.DEBUG ? Constants.DEBUG_ACCOUNT_LOGIN : "");
		mPwd.setText(!StringUtils.isEmpty(mPassword) ? mPassword : Constants.DEBUG ? Constants.DEBUG_ACCOUNT_PASSWD : "");
		
		mEmail.addTextChangedListener(mWatcher);
		mPwd.addTextChangedListener(mWatcher);
		
		if (!mRequestNewAccount) mEmail.setEnabled(false);
		checkValideLoginForm();
		
		mError.setVisibility(View.GONE);
		mProgress.setVisibility(View.INVISIBLE);
		mSubmit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				submit();
			}
		});
	}
	
	private void submit() {
		mError.setVisibility(View.GONE);
		String email = mEmail.getText().toString();
		String pwd = mPwd.getText().toString();
		
		if (mRequestNewAccount && !isAvaibleAccount(email)) {
			showAccountExist(email);
			return;
		}
		
		if (mRequestNewAccount) mUsername = email;
		mPassword = pwd;
		
		if (!TextUtils.isEmpty(mUsername) && !TextUtils.isEmpty(mPassword)) {
			mAuthTask = new LoginTask(mUsername, mPassword);
			mAuthTask.execute();
		}
	}
	
	TextWatcher mWatcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) { }
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
		@Override
		public void afterTextChanged(Editable s) {
			checkValideLoginForm();
			mError.setVisibility(View.GONE);
		}
	};
	
	private void checkValideLoginForm() {
		mSubmit.setEnabled(!StringUtils.isEmpty(mEmail.getText().toString()) && !StringUtils.isEmpty(mPwd.getText().toString()));
	}
	
	private boolean isAvaibleAccount(String username) {
		Account[] accs = mAccountManager.getAccountsByType(Constants.ACCOUNT_TYPE);
		for (int i=0; i<accs.length; i++) {
			if (username.equals(accs[i].name)) return false;
		}
		return true;
	}
	
	private void showAccountExist(String username) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Compte existant");
		builder.setMessage("Le compte '" + username + "' est d�j� configur� sur votre p�riph�rique.\nVous ne pouvez l'ajouter une seconde fois.");
		builder.setNeutralButton("Fermer", null);
		builder.create().show();
	}
	
	private class LoginTask extends AsyncTask<Void, Void, Boolean> {
		
		private Bundle mData;
		
		public LoginTask(String usr, String pwd) {
			mData = new Bundle();
			mData.putString(Api.UserColumns.EMAIL, usr);
			mData.putString(Api.UserColumns.PASSWORD, pwd);
			Log.i(TAG+".LoginTask", "create: " + mData);
		}

		@Override
		protected void onPreExecute() {
			mEmail.setEnabled(false);
			mPwd.setEnabled(false);
			mSubmit.setEnabled(false);
			mProgress.setVisibility(View.VISIBLE);
		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			String token;
			try {
				Log.i(TAG+".LoginTask", "doInBackground: " + mData.getString(Api.UserColumns.EMAIL) + " / " + mData.getString(Api.UserColumns.PASSWORD));
				token = Api.getInstance().authentificate(mData);
				mData.putString(Api.UserColumns.TOKEN, token);
				if (!TextUtils.isEmpty(token)) {
					Bundle data = Api.getInstance().loadUser(token);
					if (!data.isEmpty()) mData.putAll(data);
				}
				return Boolean.valueOf(!TextUtils.isEmpty(token));
			} catch (AuthenticatorException e) {
				mProgress.setVisibility(View.INVISIBLE);
				mError.setText(e.getMessage());
				mError.setVisibility(View.VISIBLE);
			} catch (Exception e) {
				Log.e(TAG, "LoginTask.doInBackground: failed to authenticate");
				Log.e(TAG, e.toString());
			}
			return Boolean.valueOf(false);
		}
		
		@Override
		protected void onPostExecute(final Boolean result) {
			onAuthorizationResult(mData);
		}
		
		@Override
		protected void onCancelled() {
			onAuthorizationCancel();
		}
	}

	public void onAuthorizationResult(final Bundle user) {
		final String token = user.getString(Api.UserColumns.TOKEN);
		final boolean success = !TextUtils.isEmpty(token);
		Log.i(TAG, "onAuthenticationResult(" + success + " / " + token + ")");
		if (success) Log.v(TAG, "user: " + user);
		mAuthTask = null;
		mEmail.setEnabled(true);
		mPwd.setEnabled(true);
		mSubmit.setEnabled(true);
		mProgress.setVisibility(View.INVISIBLE);
		if (success) {
			if (!mConfirmCredentials) {
				finishLogin(user);
			} else {
				finishConfirmCredentials(user);
			}
		} else {
			Log.e(TAG, "onAuthenticationResult: failed to authenticate");
		}
	}
	public void onAuthorizationCancel() {
		Log.i(TAG, "onAuthorizationCancel()");
		mAuthTask = null;
	}
	
	private void finishConfirmCredentials(final Bundle user) {
		if (Constants.DEBUG) {
			Log.i(TAG, "finishConfirmCredentials()");
			Log.i(TAG, "-> user: " + user);
		}
		final Account account = AccountHelper.getAccountByTypeAndName(Constants.ACCOUNT_TYPE, user.getString(Api.UserColumns.NAME));
		mAccountManager.setPassword(account, user.getString(Api.UserColumns.PASSWORD));
		final Intent intent = new Intent();
		intent.putExtra(AccountManager.KEY_BOOLEAN_RESULT, true);
		setAccountAuthenticatorResult(intent.getExtras());
		setResult(RESULT_OK, intent);
		finish();
	}
	
	private void finishLogin(final Bundle user) {
		final String name = user.getString(Api.UserColumns.NAME);
		final String token = user.getString(Api.UserColumns.TOKEN);
		final String pwd = user.getString(Api.UserColumns.PASSWORD);
		user.remove(Api.UserColumns.PASSWORD);
		
		if (Constants.DEBUG) {
			Log.i(TAG, "finishLogin()");
			Log.i(TAG, "-> user: " + user);
		}
		
		final Account account = new Account(name, Constants.ACCOUNT_TYPE);
		if (!mConfirmCredentials) {
			mAccountManager.addAccountExplicitly(account, pwd, user);
			mAccountManager.setAuthToken(account, Constants.ACCOUNT_TOKEN_TYPE, token);

			ContentResolver.setSyncAutomatically(account, Constants.PROVIDER_CALENDAR, true);
			//ContentResolver.setSyncAutomatically(account, ProductsProvider.AUTHORITY, true);
			
			long delay = 2628000;
			Bundle extras = new Bundle();
			extras.putLong("periodic_sync", delay);
			// TODO: enabled
			ContentResolver.addPeriodicSync(account, Constants.PROVIDER_CALENDAR, extras, delay);
			//ContentResolver.addPeriodicSync(account, ProductsProvider.AUTHORITY, extras, delay);
		} else {
			mAccountManager.setPassword(account, pwd);
		}

		final Intent intent = new Intent();
		intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, name);
		intent.putExtra(AccountManager.KEY_AUTHTOKEN, user.getString(Api.UserColumns.TOKEN));
		intent.putExtra(AccountManager.KEY_PASSWORD, user.getString(Api.UserColumns.PASSWORD));
		intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, Constants.ACCOUNT_TYPE);
		setAccountAuthenticatorResult(intent.getExtras());
		setResult(RESULT_OK, intent);
		finish();
	}
}
