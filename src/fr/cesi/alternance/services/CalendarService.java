package fr.cesi.alternance.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kolapsis.utils.HttpData;
import com.kolapsis.utils.HttpData.HttpDataException;

import fr.cesi.alternance.Constants;
import fr.cesi.alternance.api.Api;
import fr.cesi.alternance.helpers.AccountHelper;
import fr.cesi.alternance.helpers.CalendarHelper;

import android.accounts.Account;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Service;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class CalendarService extends Service {

	public static final String TAG 							= Constants.APP_NAME + ".CalendarSyncService";

	private static SyncAdapterImpl sSyncAdapter = null;

	private static class SyncAdapterImpl extends AbstractThreadedSyncAdapter {

		private Context mContext;

		public SyncAdapterImpl(Context context) {
			super(context, true);
			mContext = context;
		}

		@Override
		public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
			try {
				CalendarService.performSync(mContext, account, extras, authority, provider, syncResult);
			} catch (OperationCanceledException e) {
				Log.e("AuthService", "OperationCanceledException: " + e.getMessage(), e);
			}
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return getSyncAdapter().getSyncAdapterBinder();
	}

	private SyncAdapterImpl getSyncAdapter() {
		if (sSyncAdapter == null) sSyncAdapter = new SyncAdapterImpl(this);
		return sSyncAdapter;
	}

	private static void performSync(Context context, Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) throws OperationCanceledException {
		Log.i("AuthService", "Calendar Service performSync " + account.name);
		
		CalendarHelper.setContext(context);
		try {
			final String authtoken = AccountHelper.blockingGetAuthToken(account, Constants.ACCOUNT_TOKEN_TYPE, false);
			if (authtoken != null) {
				List<CalendarHelper.Planning> pLocals  = CalendarHelper.Planning.select(account);
				List<CalendarHelper.Planning> pRemotes = Api.getInstance().load(CalendarHelper.Planning.class, account);
				for (CalendarHelper.Planning pLocal : pLocals) {
					int index = pRemotes.indexOf(pLocal);
					if (index == -1) {
						Log.v(TAG, "-> remove local calendar: " + pLocal.getId() + ", " + pLocal.getSourceId());
						CalendarHelper.Planning.delete(account, pLocal.getId());
					}
				}
				for (CalendarHelper.Planning pRemote : pRemotes) {
					int pIndex = pLocals.indexOf(pRemote);
					Log.v(TAG, pRemote.getName());
					long calId = 0;
					if (pIndex > -1) {
						CalendarHelper.Planning pLocal = pLocals.get(pIndex);
						pRemote.setId(pLocal.getId());
						calId = CalendarHelper.Planning.update(account, pRemote);
						Log.v(TAG, "-> updated id: " + pRemote.getId());
					} else {
						calId = CalendarHelper.Planning.insert(account, pRemote);
						pRemote.setId(calId);
						Log.v(TAG, "--> inserted id: " + pRemote.getId());
					}
					if (calId > 0) {
						List<CalendarHelper.Event> eLocals  = CalendarHelper.Event.select(account, calId);
						List<CalendarHelper.Event> eRemotes = pRemote.getEvents();
						for (CalendarHelper.Event eLocal : eLocals) {
							int index = eRemotes.indexOf(eLocal);
							if (index == -1) {
								Log.v(TAG, "remove local event: " + eLocal.getId() + ", " + eLocal.getSourceId());
								CalendarHelper.Event.delete(account, eLocal.getId());
							}
						}
						for (CalendarHelper.Event eRemote : eRemotes) {
							int eIndex = eLocals.indexOf(eRemote);
							long evtId = 0;
							if (eIndex > -1) {
								CalendarHelper.Event eLocal = eLocals.get(eIndex);
								eRemote.setId(eLocal.getId());
								evtId = CalendarHelper.Event.update(account, eRemote);
								Log.v(TAG, "--> update id: " + eRemote.getId());
							} else {
								evtId = CalendarHelper.Event.insert(account, calId, eRemote);
								eRemote.setId(evtId);
								Log.v(TAG, "--> inserted id: " + eRemote.getId());
							}
						}
					}
				}
			}
		} catch (AuthenticatorException e) {
			Log.e("CalendarService", "AuthenticatorException: " + e.getMessage(), e);
		} catch (IOException e) {
			Log.e("CalendarService", "IOException: " + e.getMessage(), e);
		}
		
		/*
		try {
			//String url = "http://www.google.com/calendar/feeds/" + Constants.DEBUG_CALENDAR_REFERENCE + "@group.calendar.google.com/public/full?alt=json";
			String url = Constants.BASE_API_URL + "/calendar";
			HttpData request = new HttpData(url).get();
			JSONObject json = request.asJSONObject();
			//Log.v(TAG, "json: " + json);
			JSONObject feed = json.getJSONObject("feed");
			String title = feed.getJSONObject("title").getString("$t");
			long calId = CalendarHelper.select(account, title);
			//Log.v(TAG, "title: " + title);
			if (calId == 0) calId = CalendarHelper.insert(account, title, Constants.DEBUG_CALENDAR_REFERENCE);
			JSONArray entries = feed.getJSONArray("entry");
			//Log.v(TAG, entries.length() + " entries");
			for (int i=0; i<entries.length(); i++) {
				JSONObject entry = entries.getJSONObject(i);
				String sourceId = entry.getJSONObject("id").getString("$t");
				int index = sourceId.lastIndexOf("/");
				sourceId = sourceId.substring(index+1);
				title = entry.getJSONObject("title").getString("$t");
				String content = entry.getJSONObject("content").getString("$t");
				JSONObject dates = entry.getJSONArray("gd$when").getJSONObject(0);
				String begin = dates.getString("startTime");
				String end = dates.getString("endTime");
				String where = entry.getJSONArray("gd$where").getJSONObject(0).getString("valueString");
				long evtId = CalendarHelper.Event.select(account, calId, sourceId);
				if (evtId == 0) CalendarHelper.Event.insert(account, calId, sourceId, title, content, begin, end, where);
			}
		} catch (HttpDataException e) {
			Log.e("AuthService", "HttpDataException: " + e.getMessage(), e);
		} catch (JSONException e) {
			Log.e("AuthService", "JSONException: " + e.getMessage(), e);
		}
		*/
	}

}
